#ifndef FORMAGEOMETRICA_HPP
#define FORMAGEOMETRICA_HPP

#include <iostream>
#include <string>
#include <cmath>

class FormaGeometrica {
private:
    std::string tipo;
    float base;
    float altura;
public:
    FormaGeometrica();
    FormaGeometrica(float base, float altura);
    FormaGeometrica(std::string tipo, float base, float altura);
    ~FormaGeometrica();
    void set_tipo(std::string tipo);
    std::string get_tipo();
    void set_base(float base);
    float get_base();
    void set_altura(float altura);
    float get_altura();
    virtual float calcula_area();
    virtual float calcula_perimetro();
};

#endif