#ifndef TRIANGULORETANGULO_HPP
#define TRIANGULORETANGULO_HPP

#include "formageometrica.hpp"
#include <iostream>
#include <string>
#include <cmath>

class TrianguloRetangulo : public FormaGeometrica{
public:
    TrianguloRetangulo();
    TrianguloRetangulo(std::string tipo);
    ~TrianguloRetangulo();
    float calcula_area();
    float calcula_perimetro();
};

#endif