#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include "formageometrica.hpp"
#include <iostream>
#include <string>
#include <cmath>

class Paralelogramo : public FormaGeometrica{
private:
    float angulo_agudo;
public:
    Paralelogramo();
    Paralelogramo(std::string tipo);
    ~Paralelogramo();
    float get_angulo_agudo();
    void set_angulo_agudo(float angulo_agudo);
    float calcula_area();
    float calcula_perimetro();
};

#endif