#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include "formageometrica.hpp"
#include <iostream>
#include <string>
#include <cmath>

class Hexagono : public FormaGeometrica{
public:
    //base = lado;
    Hexagono();
    Hexagono(std::string tipo);
    ~Hexagono();
    float calcula_area();
    float calcula_perimetro();
};

#endif