#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include "formageometrica.hpp"
#include <iostream>
#include <string>
#include <cmath>

class Quadrado : public FormaGeometrica{  
public:
    Quadrado();
    Quadrado(std::string tipo);
    ~Quadrado();
};

#endif