#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include "formageometrica.hpp"
#include <iostream>
#include <string>
#include <cmath>

class Pentagono : public FormaGeometrica{
private:
    float apotema;
public:
    //base = lado
    Pentagono();
    Pentagono(std::string tipo);
    ~Pentagono();
    float get_apotema();
    void set_apotema(float apotema);
    float calcula_area();
    float calcula_perimetro();
};

#endif