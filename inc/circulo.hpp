#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include "formageometrica.hpp"
#include <iostream>
#include <string>
#include <cmath>

class Circulo : public FormaGeometrica{
private:
    float raio;
public:
    Circulo();
    Circulo(std::string tipo);
    ~Circulo();
    float get_raio();
    void set_raio(float raio);
    float calcula_area();
    float calcula_perimetro();
};

#endif