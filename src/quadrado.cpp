#include "quadrado.hpp"
#include <iostream>
#include <string>
#include <cmath>

Quadrado::Quadrado()
{
    set_tipo("Quadrado");
    set_base(3.0);
    set_altura(get_base());
}
Quadrado::Quadrado(std::string tipo)
{
    set_tipo(tipo);
    set_base(1.0);
    set_altura(get_base());
}
Quadrado::~Quadrado()
{
    
}