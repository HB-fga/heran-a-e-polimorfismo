#include "pentagono.hpp"
#include <iostream>
#include <string>
#include <cmath>

Pentagono::Pentagono()
{
    set_tipo("Pentagono");
    set_base(6.0);
    set_apotema(4.0);
}
Pentagono::Pentagono(std::string tipo)
{
    set_tipo(tipo);
    set_base(6.0);
    set_apotema(4.0);
}
Pentagono::~Pentagono()
{
    
}
float Pentagono::get_apotema()
{
    return apotema;
}
void Pentagono::set_apotema(float apotema)
{
    if(apotema < 0)
        throw(1);
    else
        this->apotema = apotema;
}
float Pentagono::calcula_area()
{
    return ((get_base()/2)*get_apotema()*5);
}
float Pentagono::calcula_perimetro()
{
    return (get_base()*5);
}