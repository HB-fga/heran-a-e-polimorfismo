#include "formageometrica.hpp"
#include "quadrado.hpp"
#include "trianguloretangulo.hpp"
#include "circulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"

#include <iostream>
#include <string>
#include <cmath>
#include <vector>

using namespace std;

int main() {

    vector <FormaGeometrica *> figuras;

    figuras.push_back(new Quadrado());
    figuras.push_back(new Paralelogramo());
    figuras.push_back(new TrianguloRetangulo());
    figuras.push_back(new Circulo());
    figuras.push_back(new Pentagono());
    figuras.push_back(new Hexagono());

    std::cout<<"###################################"<<std::endl;
    for(FormaGeometrica * forma: figuras)
    {
        std::cout<<"Tipo: "<<forma->get_tipo()<<std::endl;
        std::cout<<"Area: "<<forma->calcula_area()<<std::endl;
        std::cout<<"Perimetro: "<<forma->calcula_perimetro()<<std::endl;
        std::cout<<"###################################"<<std::endl;
    }

    return 0;
}