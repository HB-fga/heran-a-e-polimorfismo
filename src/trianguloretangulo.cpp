#include "trianguloretangulo.hpp"
#include <iostream>
#include <string>
#include <cmath>

TrianguloRetangulo::TrianguloRetangulo()
{
    set_tipo("TrianguloRetangulo");
    set_base(3.0);
    set_altura(2.0);
}
TrianguloRetangulo::TrianguloRetangulo(std::string tipo)
{
    set_tipo(tipo);
    set_base(1.0);
    set_altura(1.0);
}
TrianguloRetangulo::~TrianguloRetangulo()
{
    
}
float TrianguloRetangulo::calcula_area()
{
    return (get_base()*get_altura())/2;
}
float TrianguloRetangulo::calcula_perimetro()
{
    return get_base()+get_altura()+hypot(get_base(), get_altura());
}