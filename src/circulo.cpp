#include "circulo.hpp"
#include <iostream>
#include <string>
#include <cmath>

#define PI 3.14159265

Circulo::Circulo()
{
    set_tipo("Circulo");
    set_raio(3.0);
}
Circulo::Circulo(std::string tipo)
{
    set_tipo(tipo);
    set_raio(1.0);
}
Circulo::~Circulo()
{
    
}
float Circulo::get_raio()
{
    return raio;
}
void Circulo::set_raio(float raio)
{
    if(raio < 0)
        throw(1);
    else
        this->raio = raio;
}
float Circulo::calcula_area()
{
    return PI*raio*raio;
}
float Circulo::calcula_perimetro()
{
    return 2*PI*raio;
}