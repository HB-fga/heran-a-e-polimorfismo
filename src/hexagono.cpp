#include "hexagono.hpp"
#include <iostream>
#include <string>
#include <cmath>

Hexagono::Hexagono()
{
    set_tipo("Hexagono");
    set_base(2.0);
}
Hexagono::Hexagono(std::string tipo)
{
    set_tipo(tipo);
    set_base(2.0);
}
Hexagono::~Hexagono()
{
    
}
float Hexagono::calcula_area()
{
    return ((3*sqrt(3)*get_base()*get_base())/2);
}
float Hexagono::calcula_perimetro()
{
    return get_base()*6;
}