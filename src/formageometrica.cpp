#include "formageometrica.hpp"
#include <iostream>

FormaGeometrica::FormaGeometrica(){
    tipo = "Genérico";
    base = 1.0f;
    altura = 1.0f;
}
FormaGeometrica::FormaGeometrica(float base, float altura){
    tipo = "Genérico";
    this->base = base;
    this->altura = altura;
}
FormaGeometrica::FormaGeometrica(std::string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}
FormaGeometrica::~FormaGeometrica(){
    std::cout << "Destruindo o objeto: " << tipo << std::endl;
}
void FormaGeometrica::set_tipo(std::string tipo){
    this->tipo = tipo;
}
std::string FormaGeometrica::get_tipo(){
    return tipo;
}
void FormaGeometrica::set_base(float base){
    if(base < 0)
        throw(1);
    else
        this->base = base;
}
float FormaGeometrica::get_base(){
    return base;
}
void FormaGeometrica::set_altura(float altura){
    if(altura < 0)
        throw(1);
    else
        this->altura = altura;
}
float FormaGeometrica::get_altura(){
    return altura;
}
float FormaGeometrica::calcula_area(){
    return base * altura;
}
float FormaGeometrica::calcula_perimetro(){
    return 2*base + 2*altura;
}