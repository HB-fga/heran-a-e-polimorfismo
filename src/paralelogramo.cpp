#include "paralelogramo.hpp"
#include <iostream>
#include <string>
#include <cmath>

#define PI 3.14159265

Paralelogramo::Paralelogramo()
{
    set_tipo("Paralelogramo");
    set_base(7.0);
    set_altura(2.0);
    set_angulo_agudo(45);
}
Paralelogramo::Paralelogramo(std::string tipo)
{
    set_tipo(tipo);
    set_base(1.0);
    set_altura(1.0);
    set_angulo_agudo(45);
}
Paralelogramo::~Paralelogramo()
{
    
}
float Paralelogramo::get_angulo_agudo()
{
    return angulo_agudo;
}
void Paralelogramo::set_angulo_agudo(float angulo_agudo)
{
    if(angulo_agudo < 0)
        throw(1);
    else
        this->angulo_agudo = angulo_agudo;
}
float Paralelogramo::calcula_area()
{
    return (get_base()*get_altura());
}
float Paralelogramo::calcula_perimetro()
{
    return (get_base()*2 + pow((get_altura()/(sin(get_angulo_agudo()*PI/180))), 2)*2);
}